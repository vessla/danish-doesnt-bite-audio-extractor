let audioUrls = [];
let downloadButton = document.getElementById('download');

downloadButton.innerText = chrome.i18n.getMessage("downloadButton");
downloadButton.onclick = function(element) {
	console.log("Download All! invoked");
	let audioCounter = 0;
	let timeoutMultiplier = 0;
	for (const audioUrl of audioUrls) {
		 //Immediate Invoking Function Expression (https://borgs.cybrilla.com/tils/javascript-for-loop-with-delay-in-each-iteration-using-iife/)
		 (function () {
		 setTimeout(function () {
			try{
				chrome.downloads.download({url: audioUrl},
                                  function(id) {
									  audioCounter++;
									  if(audioCounter == audioUrls.length){
										window.close();	
									  }
								  }
				);
			} catch (exception) {
				console.log(exception.message);
			}
		}, 1000*timeoutMultiplier);
		})();
		timeoutMultiplier++;
	}
}

window.addEventListener('DOMContentLoaded', function () {
  // ...query for the active tab...
  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function (tabs) {
    // ...and send a request for the download links...
    chrome.tabs.sendMessage(
        tabs[0].id,
        {from: 'popup', subject: 'getDownloadLinks'},
        // callback to be called from the receiving end (content script)
        function(downloadLinks){
			console.log('Received answer from content.js');
			if(downloadLinks === undefined || downloadLinks.length == 0){
				downloadButton.disabled = true;
			}
			else{
				downloadButton.disabled = false;
				audioUrls = downloadLinks;
			}
		});
	});
});