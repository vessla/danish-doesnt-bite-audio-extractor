chrome.runtime.onInstalled.addListener(function() {
    chrome.storage.sync.set({contentSearchSelectors: {'source[src$=".mp3"]': 'src'}}, function() {
      console.log("Content search selectors initialized");
    });
});

chrome.runtime.onMessage.addListener(function (msg, sender, response) {
    if (msg.action === "updateIcon") {
		console.log('updateIcon: '+msg.value);
        if (msg.value == true) {
			chrome.browserAction.setIcon({tabId: sender.tab.id, path: "images/icon_active.png"});
        } else {
            chrome.browserAction.setIcon({tabId: sender.tab.id, path: "images/icon_inactive.png"});
        }
    }
});