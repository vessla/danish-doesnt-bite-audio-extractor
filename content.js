let downloadLinks = [];
prepareDownloadLinks();

function prepareDownloadLinks() {
	chrome.storage.sync.get('contentSearchSelectors', function(data) {
		console.log(data.contentSearchSelectors);
	
		// REQUIRES ECMASCRIPT 2015+ (https://wesbos.com/let-vs-const/)
		for (const selectorQuery of Object.keys(data.contentSearchSelectors)) {
			/*
			With JQuery initialize like shown below:
			var downloadLinks = ($( "source[src$='.mp3']" )).map(function() {
			return this.src;
			});
			*/
		
			/*
			[... ] is ES6 spread operator used to convert StaticNodeList returned by querySelectorAll() (done in order to use map function)
			*/
			downloadLinks = downloadLinks.concat([...document.querySelectorAll(selectorQuery)].map(sourceTag => sourceTag.getAttribute(data.contentSearchSelectors[selectorQuery])));
		}
		
		console.log(downloadLinks);
		chrome.runtime.sendMessage({
			action: 'updateIcon',
			value: downloadLinks.length>0
		});
	});
}

chrome.runtime.onMessage.addListener(function (msg, sender, response) {
		console.log('Received: '+msg.subject+' from '+msg.from);
		if ((msg.from === 'popup') && (msg.subject === 'getDownloadLinks')) {
			response(downloadLinks);
        }
});
