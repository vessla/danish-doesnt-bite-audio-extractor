# README #

### What is this repository for? ###

This application is a working prototype stage chrome extension, and has been created for private use as a tool to support language learning process. It allows to archive the additional audio files for the "Duński nie gryzie!" danish course.

The extension allows to replace the original audio file URL selector in order to detect appropriate links on different websites. Currently this needs to be done programmatically, by adding new expressions to the [background.js](https://bitbucket.org/vessla/danish-doesnt-bite-audio-extractor/src/3840c64f886421c8c0ab4a32f20f36187eb6e9c4/background/background.js?at=master&fileviewer=file-view-default) file, however additional extension configuration page may also be added in future.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND

### Who do I talk to? ###

pjadamska@gmail.com